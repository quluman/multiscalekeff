from scipy.integrate import quad, dblquad, tplquad
import numpy as np
from numpy import sin, cos, abs, sqrt, pi, exp, conjugate
import matplotlib.pyplot as plt
import parameter3d as pm
import plot_112_A
import plot_112_K
import plot_112_M




T = 450 # Temperature: K
model = 'kittel'   # Kittel only, no data for ried-like model

kb = 1.3806488 * 1e-23 # J/K
#S = 7.5  # Spin
bohr = 9.274009682 * 1e-24 # Bohr magneton: J/T
mu0 = 4*pi*1e-7   # N/A^2
meV = 1.6*1e-22

# Lattice parameter
a = 0.849 #e-9 # lattice parameter: nm
b = 0.849 #e-9 # lattice parameter: nm
c = 0.492 #e-9 # lattice parameter: nm
V = a*b*c

# taken from Ried Paper
#M_T = 1.09 * 1e6  # A/m
#M_R = 0.18 * 1e6  # A/m
#M_exp = (M_T + M_R)*a*b*c*1e-27   # A*m^2
# M_exp may be multiplied with L^3 instead of V

A_300 = 6.7*1e-12 # J/m at 300K
A_450 = 5.7*1e-12 # J/m at 450K

if T==300:
    A = A_300
    #K = K_300*(a**3)
    M_s = 1.66 / mu0
    H_eff = 7.99

if T==450:
    A = A_450
    #K = K_450*(a**3)
    M_s = 1.44 / mu0
    H_eff = 4.76    


def calculate(H,length):

    pm.H = H   
    
    #M_exp = (M_T + M_R)*(length**3)*(c/a)*1e-27
    
    limit = pi/length     # Here length is in nm
    limitz = pi*(a/c)/length
    
    print('H = ',pm.H)
    
    integral = tplquad(integrand, 0, limit, lambda qx: 0, lambda qx: limit,
                                      lambda qx,qy: 0, lambda qx,qy: limitz)[0]

    delta_m = 8*2*bohr* integral*(V/((2*pi)**3)) 
    
    #ratio = 1 + delta_m / (M_exp +delta_m)

    ratio = 1 + delta_m/(M_s*V*1e-27)

    return np.asarray([length, ratio, ratio**2, ratio**3])    
    #return np.asarray([length, ratio, ratio**2, ratio**3, delta_m, H, integral])



def Ek_old(qz,qy,qx):    
    return ((0.76-0.125*pm.H)+1.073*(qx**2+qy**2+qz**2))*1.6*1e-22   # energy: J
    #return ((0.82-0.125*pm.H)+1.328*(qx**2+qy**2+qz**2))*1.6*1e-22

def Ek(qz,qy,qx):
    return (4*A/M_s)*bohr*1e18*(qx**2+qy**2+qz**2) + 2*(-pm.H+H_eff)*bohr


def integrand(qz,qy,qx):
    return 1/(exp(Ek(qz,qy,qx)/(kb*T))-1)


def main():
    
    for j in range(5):
        
        result = []
        
        for i in range(40):
        
            result.append(calculate(j,(i/4+1)))
            
        
        results = np.asarray(result)
        
        filename = "output/threeD"+"_H"+str(pm.H)
        
        np.savetxt(filename, results)
        
        
        plt.xlabel('Spin-wave cutoff length/nm')
        plt.ylabel('K(H,L)/K_exp')
        plt.title('K(H,L)/K_exp as a function of SW cutoff length, T= '+str(T)+'K')
        #plt.text(20, 500, 'text')
        plt.plot(results[:,0],results[:,3],label=('H ='+str(pm.H)))
        
        plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.)
        
    #plt.show()
    

    
def makeplots():
    plot_112_M.plot_M(T,model)
    plot_112_A.plot_A(T,model)
    plot_112_K.plot_K(T,model)



main()

makeplots()











