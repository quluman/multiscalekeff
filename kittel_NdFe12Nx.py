

from scipy.integrate import quad, dblquad, tplquad
import numpy as np
from numpy import sin, cos, abs, sqrt, pi, exp, conjugate, arccos
#from mpl_toolkits.mplot3d import Axes3D
#from matplotlib import cm
#from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import matplotlib
import math




kb = 1.3806488 * 1e-23 # ( m^2*kg)/(s^2*K)
T = 300 # Temperature: K
#S = 3.5  # Spin
bohr = 9.274009682 * 1e-24 # Bohr magneton: J/T
mu0 = 4*pi*1e-7   # N/A^2
meV = 1.6*1e-22

# Lattice parameter
a = 0.849e-9 # lattice parameter: m
c = 0.492e-9 # lattice parameter: m
V = a*a*c

# taken from hmm2015.pdf
A_300 = 6.7*1e-12 # J/m at 300K
A_450 = 5.7*1e-12 # J/m at 450K

#K_300 = 5.28*1e6 # J/m^3  at 300K
#K_450 = 2.73*1e6 # J/m^3  at 450K


H_ext = 0  # external field

#H_eff = 5  # effective field ?   H_eff = 2*K/M

#M_s = 2*S * bohr / (a*a*c) # saturation magnetization ? 


if T==300:
    A = A_300
    #K = K_300*(a**3)
    M_s = 1.66 / mu0
    H_eff = 7.99

if T==450:
    A = A_450
    #K = K_450*(a**3)
    M_s = 1.44 / mu0
    H_eff = 4.76    



def theta(kx,ky,kz):
    
    #if kx+ky+kz == 0:
    #    return 0

    #else:
    return arccos(kz/sqrt(kx**2+ky**2+kz**2 + 1e-20))
   
    
"""
def theta(kx,ky,kz):
    if kz == 0:
        return 0
    else:
        return pi
"""        
        
def A_k(kx,ky,kz):
    return (4*A/M_s)*bohr*1e18*(kx**2+ky**2+kz**2) + 2*(H_ext+H_eff)*bohr #+ 4*pi*M_s*mu0*bohr*((sin(theta(kx,ky,kz)))**2)
    
def B_k(kx,ky,kz):
    #return 4*pi*M_s*mu0*bohr*((sin(theta(kx,ky,kz)))**2)
    return 0

def E_k(kx,ky,kz):
    return sqrt(A_k(kx,ky,kz)**2 - B_k(kx,ky,kz)**2)
    

def makeplot():

    resolution = 10000   
    
    result = []
    
    L = 1   # in nm
    
    for i in range(resolution+1):
            
        result.append(np.asarray([pi/L*i/resolution, E_k(0,0,(pi/L)*i/resolution)]))
        
    results_z = np.asarray(result)
    
    #print(results)

    result = []    
    
    for i in range(resolution+1):
            
        result.append(np.asarray([pi/L*i/resolution, E_k(0,(pi/L)*i/resolution,0)]))
        
    results_xy = np.asarray(result)
        
    
    
    
    plt.plot(results_z[:,0],results_z[:,1]/meV,label=('NdFe12Nx, Ek_z, T= '+str(T)),lw=2)    
    #plt.plot(results_xy[:,0],results_xy[:,1]/meV,label=('Ek_xy, T= '+str(T)))
    plt.legend(bbox_to_anchor=(0, 1), loc=2, borderaxespad=0.)
    plt.xlabel('wavenumber kz or kx [1/nm]',fontsize = 25)
    plt.ylabel('E_k [meV]',fontsize = 25)
    #plt.ylim([0,2])
    #plt.xlim([1,11])
    plt.title('E_k as a function of k',fontsize = 30)
    
    print(results_z[0,1]/meV)
    plt.show()
    


makeplot()





