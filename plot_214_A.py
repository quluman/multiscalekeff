

from scipy.integrate import quad, dblquad, tplquad
import numpy as np
from numpy import sin, cos, abs, sqrt, pi, exp, conjugate
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import matplotlib
import math



def plot_A(T,model):

    
    results=[]
    
    fig = plt.figure()
    
    for j in range(5):
        
            
        filename = "output/threeD"+"_H"+str(j)
    
        results.append(np.loadtxt(filename))
       
        plt.plot(results[j][:,0],results[j][:,2],label=('H = '+str(j)+' T, '+model),lw=2)
    
    
    plt.xlabel('Spin-wave cutoff length [nm]',fontsize = 25)
    plt.ylabel('A(H,L)/A_exp',fontsize = 25)
    plt.ylim([1,max(results[j][:,2])+0.01*(max(results[j][:,2])-1)])
    plt.xlim([1,10])
    plt.title('A(H,L)/A_exp vs SW cutoff length at T = '+str(T),fontsize = 30)
    #plt.text(20, 500, 'text')
    
    
    plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.,prop={'size':25})
    
    ax = fig.gca()
    ax.set_xticks(np.arange(1,10.5,0.5))
    ax.set_yticks(np.arange(1,max(results[j][:,2]),0.01))    #(max(results[j][:,3])-1)/10
    plt.grid()
    
    fig.set_size_inches(16,12)
    
    plt.savefig('A_T_'+str(T)+'_Nd2Fe14B')#, dpi=200, figsize=(1600,1200))

#plt.show()







