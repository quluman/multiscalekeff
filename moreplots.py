from scipy.integrate import odeint, simps
import numpy as np
from numpy import sin, cos, abs, sqrt, pi, exp, log
import matplotlib.pyplot as plt


data_112 = np.loadtxt('input/112_450_H2_K')

data_214 = np.loadtxt('input/214_450_H2_K')





plt.plot(data_112[:,0], data_112[:,1], color= 'r', label=('NdFe12Nx at 450K, H = 2'), lw=2)
plt.plot(data_214[:,0], data_214[:,1], label=('Nd2Fe14B at 450K, H = 2'), lw=2)


plt.xlabel('L [nm]',fontsize = 25)
plt.ylabel('M(H,L)/M(exp)',fontsize = 25)
plt.title('A plot of M(L)/M(exp) vs L',fontsize = 25)
plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.,prop={'size':20})

plt.xlim([1,10])
plt.savefig('M_T_decay')
plt.show()