from scipy.integrate import odeint, simps
import numpy as np
from numpy import sin, cos, abs, sqrt, pi, exp, log
import matplotlib.pyplot as plt

Temp = 300
kb = 1.3806488 * 1e-23 # J/K

H_ext = 0

a = 0.88 *1e-9 # lattice parameter: m
b = 0.88 *1e-9 # lattice parameter: m
c = 1.22/4* 1e-9 # lattice parameter: m
V = a*b*c


# taken from hmm2015
A_300 = 7.7*1e-12 # J/m at 300K
A_450 = 4.9*1e-12 # J/m at 450K


if Temp == 300:
    A = A_300
    #M_s = 1.61 / mu0
    #H_eff = 5.33
    
if Temp == 450:
    A = A_450
    #M_s = 1.29 / mu0
    #H_eff = 4.51    


h = H_ext/(A*(pi/a)**2)


def deriv(y,l): # return derivatives of the array y
    #a = -2.0
    #b = -0.1
    return y*(-1+y/(2*pi*(1+h*exp(2*l))))
    #return y*l

ll = np.linspace(0.0, 2.3, 1000)

Tinit = 1 # initial values

result = odeint(deriv, Tinit, ll)


ll = np.asarray(ll)

result = np.asarray(result)

J = np.empty(result.shape[0])
I = np.empty(result.shape[0])
zeta = np.empty(result.shape[0])

for i in range(result.shape[0]):
    
    J[i] = kb*Temp/(result[i]*exp(ll[i]))
    I[i] = result[i]/(2*(pi+h*exp(ll[i])))



#plt.xlabel('L [nm]',fontsize = 25)
#plt.ylabel('A(L)/A(exp)',fontsize = 25)
#plt.title('A plot of A(L)/A(exp) vs L',fontsize = 25)
#plt.text(20, 500, 'text')
plt.plot(exp(ll), J/J[0], label=('Grinstein results'),lw=2)


data_300 = np.loadtxt('input/214_H0_300K')

data_450 = np.loadtxt('input/214_H0_450K')

#plt.plot(data_300[:,0], data_300[:,2], label=('Ried at 300K'), lw=2)
#plt.plot(data_450[:,0], data_450[:,2], label=('Ried at 450K'), lw=2)

plt.xlim([1,10])
#plt.show()



#print(simps([1],[1]))

#"""

# Here I calculate the zeta integral. results are strange... identical to J/J[0] plots
for i in range(1,ll.shape[0]):


    #print(Tinit)
    zeta[i] = exp(-simps(I[0:i], ll[0:i]))

zeta[0] = 1

plt.plot(exp(ll), zeta, label=('zeta(l)'), lw=2)

#plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.,prop={'size':25})
#print(result.shape[0]==ll.shape[0])


#"""

