from scipy.integrate import quad, dblquad, tplquad
import numpy as np
from numpy import sin, cos, abs, sqrt, pi, exp, conjugate
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import matplotlib
import math
import parameter3d as pm


kb = 1.3806488 * 1e-23 # J/K
T = 300 # Temperature: K
#S = 7.5  # Spin
bohr = 9.274009682 * 1e-24 # Bohr magneton: J/T
mu0 = 4*pi*1e-7   # N/A^2
S = 9.5

# Lattice parameter
a = 0.8 #e-9 # lattice parameter: nm
b = 0.8 #e-9 # lattice parameter: nm
c = 1.2 #e-9 # lattice parameter: nm
V = a*b*c

# taken from Ried Paper
M_T = 1.09 * 1e6  # A/m
M_R = 0.18 * 1e6  # A/m

M_exp = (M_T + M_R)*a*b*c*1e-27   # A*m^2


def calculate(H,length):

    pm.H = H   
    
    
    limit = pi/length     # Here length is in nm
    limitz = pi*(a/c)/length
    
    print('H = ',pm.H)
    
    integral = tplquad(integrand, 0, limit, lambda qx: 0, lambda qx: limit,
                                      lambda qx,qy: 0, lambda qx,qy: limitz)[0]

    delta_m = 8*2*bohr* integral*(V/((2*pi)**3)) 
    
    #ratio = 1 + delta_m / (M_exp +delta_m)

    ratio = 1 + delta_m/M_exp 
    
    

    return np.asarray([length, ratio, ratio**2, ratio**3, delta_m, H, integral])
    
    



def Ek(qz,qy,qx):    
    return ((0.76-0.125*pm.H)+1.073*(qx**2+qy**2+qz**2))*1.6*1e-22   # energy: J
    #return ((0.82-0.125*pm.H)+1.328*(qx**2+qy**2+qz**2))*1.6*1e-22

def integrand(qz,qy,qx):
    return 1/(exp(Ek(qz,qy,qx)/(kb*T))-1)







#for j in range(1):
    
#    result = []
    
#    for i in range(1):
    
#        result.append(calculate(j,(i+3)))
        
    
#    results = np.asarray(result)
    
#    filename = "output/threeD"+"_H"+str(pm.H)
    
#    np.savetxt(filename, results)
    
    
#    plt.xlabel('Spin-wave cutoff length/nm')
#    plt.ylabel('K(H,L)/K_exp')
#    plt.title('K(H,L)/K_exp as a function of SW cutoff length')
    #plt.text(20, 500, 'text')
#    plt.plot(results[:,0],results[:,3],label=('H ='+str(pm.H)))
    
#    plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.)


#plt.show()

for j in range(6):
    
    pm.H = j

    result = []
    for i in range(100):
    
        
        result.append(np.asarray([pi/(i+4), integrand(0,0,pi/(i+4))]))
        
    results = np.asarray(result)
    
    print(results)
        
    plt.plot(results[:,0],results[:,1],label=('H ='+str(pm.H)))
    plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.)
    
    
plt.xlabel('k (1/nm)',fontsize = 25)
plt.ylabel('Integrand',fontsize = 25)

plt.title('Intergrand(k,H,L=4) vs k',fontsize = 30)
    
plt.show()




