

from scipy.integrate import quad, dblquad, tplquad
import numpy as np
from numpy import sin, cos, abs, sqrt, pi, exp, conjugate
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import matplotlib
import math



T = 300

results=[]

for j in range(7):
    
        
    filename = "output/threeD"+"_H"+str(j)

    results.append(np.loadtxt(filename))
   
    plt.plot(results[j][:,0],results[j][:,3],label=('H = '+str(j)+' T'))


plt.xlabel('Spin-wave cutoff length/nm',fontsize = 25)
plt.ylabel('K(H,L)/K_exp',fontsize = 25)
#plt.ylim([0,2])
#plt.xlim([1,11])
plt.title('K(H,L)/K_exp as a function of SW cutoff length T = '+str(T),fontsize = 30)
#plt.text(20, 500, 'text')


plt.legend(bbox_to_anchor=(1, 1), loc=1, borderaxespad=0.,prop={'size':25})



plt.show()







